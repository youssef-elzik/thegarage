import React from 'react';
import VerficationEmail from '../component/VerficationEmail';
import PageHelmet from "../component/common/Helmet";
import Breadcrumb from "../elements/common/Breadcrumb";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";

const VerficationEmailPage = () => {

    return(
        <>
            <PageHelmet pageTitle='Email Verfication' />
            <Header headertransparent="header--transparent" colorblack="color--black" logoname="logo.png" />
            <Breadcrumb title={'Email Verfication'}  isbread={true} />
            <main className="page-wrapper">
                <VerficationEmail />
            </main>
            <Footer />
        </>
    )
}

export default VerficationEmailPage;