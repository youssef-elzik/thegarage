import React, { useState, useEffect } from "react";
import PageHelmet from "../component/common/Helmet";
import { FiHeadphones , FiMail , FiMapPin } from "react-icons/fi";
import ContactTwo from "../elements/contact/ContactTwo";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";
import { axiosAPI } from '../helpers/config';
import ContactPageImage from '../../public/assets/images/pageheader/conatct3.png';

const Contact = () => {

    const [state, setState] = useState({});

    const [ phone, setPhone ] = useState([]);

    const [ email, setEmail ] = useState([]);

    useEffect(() => {
        axiosAPI.get('contactus')
        .then(res => {
            const ContactData = res.data.data
            setState(ContactData)
            setPhone(res.data.data.phone)
            setEmail(res.data.data.email)
            
        })
    }, [])
        return(
            <>
                <PageHelmet pageTitle='Contact Us' />
                <Header headertransparent="header--transparent" colorblack="color--black" logoname="logo.png" /> 
                <div className="rn-page-title-area pt--120 pb--190 bg_image"  data-black-overlay="5" style={{backgroundImage : `url(${ContactPageImage})`}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title" style={{color: '#a60808'}}>CONTACT US</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="rn-contact-top-area ptb--120 bg_color--5">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div className="rn-address">
                                    <div className="icon">
                                        <FiHeadphones />
                                    </div>
                                    <div className="inner">
                                        <h4 className="title">Contact With Phone Number</h4>
                                        {
                                            phone.map((item,index) => (
                                                <p key={index}><a href={`tel:${item}`}>{item}</a></p>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6 col-12 mt_mobile--50">
                                <div className="rn-address">
                                    <div className="icon">
                                        <FiMail />
                                    </div>
                                    <div className="inner">
                                        <h4 className="title">Email Address</h4>
                                        {
                                            email.map((item,index) => (
                                                <p key={index}><a href={`mailto:${item}`}>{item}</a></p>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6 col-12 mt_md--50 mt_sm--50">
                                <div className="rn-address">
                                    <div className="icon">
                                        <FiMapPin />
                                    </div>
                                    <div className="inner">
                                        <h4 className="title">Location</h4>
                                        <p>{state.address}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="rn-contact-page ptb--120 bg_color--1">
                    <ContactTwo />
                </div>
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                <Footer />
                
            </>
        )
    }
export default Contact