import React, { useState } from "react";
import { axiosAPI } from '../../helpers/config'
// import {FaTwitter ,FaInstagram ,FaFacebookF , FaLinkedinIn} from "react-icons/fa";


const ContactTwo = () => {

    const [state, setState] = useState({
        rnName: '',
        rnEmail: '',
        rnSubject: '',
        rnMessage: '',
    })

    const [error, setError] = useState({
        fname: "",
        email: "",
        subject: ""
    })

    const [ success, setSuccess ] = useState(false);

    const handleValidation = () => {

        let valid = true

        if(state.rnName === ""){
            setError((old) => ({...old, fname : "Your Name is Required"}))
            valid = false
        }
        if(state.rnEmail === ""){
            setError((old) => ({...old, email : "Your Email is Required"}))
            valid = false
        }
        if(state.rnSubject === ""){
            setError((old) => ({...old, subject : "Your Subject is Required"}))
            valid = false
        }
        if(!state.rnEmail.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            setError((old) => ({...old, email : "The email must be a valid email address."}))
            valid = false
        }

        return valid;
        
    }
    
    const HandelSubmit = (e) => {
        e.preventDefault()
        if(handleValidation()) {
            axiosAPI.post('contactus',
                {
                    name: state.rnName,
                    email: state.rnEmail,
                    subject: state.rnSubject,
                    message: state.rnMessage 
                }
            )
            .then(res => {
                if(res.data.status_code === 200) {
                    setSuccess(true);
                    setState(() => ({ 
                        rnName: "",
                        rnEmail: "",
                        rnSubject: "",
                        rnMessage: "",
        
                    }))
                }
            })

        }
            else {
        }
    }
        return(
            <div className="contact-form--1">
                <div className="container">
                    <div className="row row--35 align-items-center">
                        <div className="col-lg-6 order-2 order-lg-1 pr-0">
                            <div className="form-container">
                                <div className="section-title text-left mb--50" style={{paddingTop: "18px"}}>
                                    <h2 className="title">Contact Us</h2>
                                    <p className="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto cupiditate aperiam neque.</p>
                                </div>
                                <div className="form-wrapper">
                                    <form onSubmit={HandelSubmit}>
                                        <label htmlFor="item01">
                                            <input
                                                type="text"
                                                name="name"
                                                id="item01"
                                                className="cus-input"
                                                value={state.rnName}
                                                onChange={(e)=>{
                                                    const text = e.target.value
                                                    setState((old) => ({...old, rnName: text}))
                                                    setError((old) => ({...old, fname : ""}))
                                                    
                                                }}
                                                placeholder="Your Name *"
                                            />
                                            {
                                                (error.fname) ? ( <small className="alert  alert-danger">{error.fname}</small>) : null
                                            }
                                        </label>
                                        <label htmlFor="item02">
                                            <input
                                                type="text"
                                                name="email"
                                                id="item02"
                                                className="cus-input"
                                                value={state.rnEmail}
                                                onChange={(e)=>{
                                                    const text = e.target.value
                                                    setState( (old) => ({...old ,rnEmail: text}))
                                                    setError((old) => ({...old, email : ""}))
                                                }}
                                                placeholder="Your email *"
                                            />
                                            {
                                                (error.email) ? ( <small className="alert  alert-danger">{error.email}</small>) : null
                                            }
                                        </label>
                                        <label htmlFor="item03">
                                            <input
                                                type="text"
                                                name="subject"
                                                id="item03"
                                                className="cus-input"
                                                value={state.rnSubject}
                                                onChange={(e)=>{
                                                    const text = e.target.value
                                                    setState((old) => ({...old, rnSubject: text}))
                                                    setError((old) => ({...old, subject : ""}))
                                                }}
                                                placeholder="Write a Subject *"
                                            />
                                            {
                                                (error.subject) ? ( <small className="alert  alert-danger">{error.subject}</small>) : null
                                            }
                                        </label>
                                        <label htmlFor="item04">
                                            <textarea
                                                type="text"
                                                id="item04"
                                                className="cus-input"
                                                name="message"
                                                value={state.rnMessage}
                                                onChange={(e)=>{
                                                    const text = e.target.value
                                                    setState((old) => ({...old, rnMessage: text}))
                                                }}
                                                placeholder="Your Message"
                                            />
                                        </label>
                                        <button className="rn-button-style--2 btn-solid" type="submit" value="submit" name="submit" id="mc-embedded-subscribe">SEND</button>
                                    </form>
                                    {
                                        success && <div class="alert alert-success text-center mt-5" role="alert">Message Sent Successfully</div>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 order-1 order-lg-2 pl-0">
                            <div className="thumbnail mb_md--30 mb_sm--30 map-ifram">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13814.055204514792!2d31.2078896!3d30.0508035!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8631219e1a7621e9!2sThe%20Garage%20sound%20Studio!5e0!3m2!1sen!2seg!4v1613568470808!5m2!1sen!2seg" title="myFrame" width="100%" height="450" frameBorder="0" style={{border:0}} allowFullScreen="" aria-hidden="false" tabIndex="0" data-fillr-frame-id="1208045895"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
export default ContactTwo;