import React from "react";
import {FaInstagram ,FaFacebookF , FaPhone, FaVimeoV} from "react-icons/fa";
import { MdLocationOn, MdEmail } from 'react-icons/md'


const ContactOne = ({address,email,phone,facebook,instagram,vimeo}) => {

    const SocialShare = [
        {Social: <FaFacebookF /> , link: facebook , title:"Facebook"},
        {Social: <FaInstagram /> , link: instagram , title:"Instagram"},
        {Social: <FaVimeoV /> , link: vimeo , title:"Vimeo"},
    ]
    return(
        <div className="contact-form--1 ">
            <div className="container">
                <div className="row row--35 align-items-start">
                <div className="col-md-6 pr-0">
                    <div className="thumbnail mb_md--30 mb_sm--30">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13814.055204514792!2d31.2078896!3d30.0508035!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8631219e1a7621e9!2sThe%20Garage%20sound%20Studio!5e0!3m2!1sen!2seg!4v1613568470808!5m2!1sen!2seg" title="myFrame" width="600" height="450" frameBorder="0" style={{border:0}} allowFullScreen="" aria-hidden="false" tabIndex="0" data-fillr-frame-id="1208045895"></iframe>
                    </div>
                </div>
                    <div className="col-md-6 px-0">
                        <div className="section-title text-left mb--50">
                            <div className="section-box">
                                <h3 className="title">Contact Us</h3>
                                <ul className="icon-list">
                                    <li><span className="icon"><MdLocationOn /></span> {address}</li>
                                    <li><span className="icon"><FaPhone /></span> <a href={`tel:${phone}`}> {phone}</a></li>
                                    <li><span className="icon"><MdEmail /></span> <a href={`mailto:${email}`}> {email}</a></li>
                                </ul>
                                <div className="inner">
                                    <ul className="social-share rn-lg-size d-flex justify-content-start liststyle">
                                        {SocialShare.map((val , i) => (
                                            <li key={i}><a href={`${val.link}`} target="_blank" rel="noopener noreferrer" title={val.title}>{val.Social}</a></li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
} 

export default ContactOne;