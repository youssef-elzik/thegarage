import ActionTypes from '../../helpers/ActionTypes';

const initialState = {
    WorksData : []

}

const Works =  (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.WORKS_PAGE_DATA:
            return{ ...state, WorksData :  action.payload}
        default:
            return state;
    }
};

export default Works;