import { axiosAPI  } from "../../helpers/config";
import ActionTypes from '../../helpers/ActionTypes';


// Handle Information Data

const InformationData = (payload) => ({
    type: ActionTypes.INFORMATION_DATA,
    payload: payload,
});

export const GetInformationDataHandler = () => {
    return async (dispatch) => {
    try {
        const { data } = await axiosAPI.get("home");
        if(data.status_code === 200){
            dispatch(InformationData(data.data));
        }

    } catch (error) {
        console.log(error.response); 
    }
    };
};