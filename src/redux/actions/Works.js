import { axiosAPI  } from "../../helpers/config";
import ActionTypes from '../../helpers/ActionTypes';


// Handle Works Page Data

const WorksPageData = (payload) => ({
    type: ActionTypes.WORKS_PAGE_DATA,
    payload: payload,
});

export const GetWorksPageDataHandler = () => {
    return async (dispatch) => {
    try {
        const { data } = await axiosAPI.get("works");
        if(data.status_code === 200){
            dispatch(WorksPageData(data.data));
        }

    } catch (error) {
        console.log(error.response); 
    }
    };
};