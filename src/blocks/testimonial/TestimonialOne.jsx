import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
const TestimonialOne = ({data}) => {
    return (
        <div className="row">
            <div className="col-lg-12">
                <Tabs>
                    {
                        data.map( item => (
                            <TabPanel key={item.id}>
                                <div className="rn-testimonial-content text-center">
                                    <div className="inner">
                                        <p>{item.opinion}</p>
                                    </div>
                                    <div className="author-info">
                                        <h6><span>{item.name} </span> - {item.position}</h6>
                                    </div>
                                </div>
                            </TabPanel>
                        ))
                    }
                    
                    <TabList className="testimonial-thumb-wrapper">
                        {
                            data.map( img => (
                                <Tab key={img.id}>
                                    <div className="testimonial-thumbnai">
                                        <div className="thumb">
                                            <img src={img.image} alt={img.name}/>
                                        </div>
                                    </div>
                                </Tab>
                            ))
                        }
                    </TabList>
                </Tabs>
            </div>
        </div>
    )
}

export default TestimonialOne
