import React from 'react'
import data from "./data";
const TeamTwo = ({teams}) => {
    return (
        <div className="row">
            {teams.map((value) => (
                <div className="col-lg-3" key={value.id}>
                    <div className={`team-static`}>
                        <div className="thumbnail">
                            <img src={value.image} alt="Blog Images"/>
                        </div>
                        <div className="inner">
                            <div className="content">
                                <h4 className="title">{value.name}</h4>
                                <p className="designation">{value.position}</p>
                            </div>
                            {/* <ul className="social-transparent liststyle d-flex" >
                                {value.socialNetwork.map((social, index) =>
                                    <li key={index}><a href={`${social.url}`}>{social.icon}</a></li>
                                )}
                            </ul> */}
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}
export default TeamTwo
