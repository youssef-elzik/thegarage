import React from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
    return (
        <div className="footer-style-2 ptb--20">
            <div className="wrapper plr--50 plr_sm--20">
                <div className="row align-items-center justify-content-between">
                    <div className="col-lg-5 col-sm-12 col-12">
                        <div className="inner">
                            <div className="logo text-lg-left text-center text-md-center text-sm-center mb_sm--20">
                                <Link to="/">
                                    <img src="/assets/images/logo/logo-light.png" alt="Logo images"/>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-7 col-sm-12 col-12">
                        <div className="inner text-lg-right text-center mt_md--20 mt_sm--20">
                            <div className="text">
                                <p>Copyright © 2021 The Garage. All Rights Reserved. Powerd By <a href="https://boldbrand.co/" title="Bold Brand" target="_blank" rel="noopener noreferrer">Bold Brand</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Footer;