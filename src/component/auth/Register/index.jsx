import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SignUpHandler } from "../../../redux/actions/Auth";
import ShowPassImage from '../../../../public/assets/images/icons/showpass.svg';
import HidePassImage from '../../../../public/assets/images/icons/hidden.svg';

const Register = () => {

    const dispatch = useDispatch();

    const [ state, setState ] = useState({
        name: "",
        email: "",
        phone:"",
        password: "",
        confirm_password: ""
    })

    const [ showPassword, setShowPassword ] = useState(false);

    const [ showConfirmPassword, setShowConfirmPassword ] = useState(false);

    const HandelShowPassword = () => {
        setShowPassword(!showPassword);
    }

    const HandelShowConfirmPassword = () => {
        setShowConfirmPassword(!showConfirmPassword);
    }

    const HandelSubmit = (e) => {
        e.preventDefault();
        dispatch(SignUpHandler(state));
    }

    return(
        <section className="auth">
            <div className="container">
                <div className="auth-body">
                    <div className="auth-header">
                        <h1 className='title'>Sign Up</h1>
                    </div>
                    <form className='auth-form' onSubmit={HandelSubmit}>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Full Name" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, name : text}))
                                }
                            } />
                        </div>
                        <div className="form-group">
                            <input type="email" className="form-control" placeholder="Email" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, email : text }))
                                }
                            } />
                        </div>
                        <div className="form-group">
                            <input type="phone" className="form-control" placeholder="Phone Number" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, phone : text }))
                                }
                            } />
                        </div>
                        <div className="form-group">
                            <input type={ showPassword ? "text" : "password" } className="form-control" placeholder="Password" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, password : text }))
                                }
                            } />
                            {
                                showPassword ? 
                                <img className='show-icon' src={HidePassImage} alt="Password Icon" onClick={HandelShowPassword} title="Hide Password" /> 
                                :
                                <img className='show-icon' src={ShowPassImage} alt="Password Icon" onClick={HandelShowPassword} title="Show Password" />
                            }
                        </div>
                        <div className="form-group">
                            <input type={ showConfirmPassword ? "text" : "password" } className="form-control" placeholder="Confirm Password" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, confirm_password : text }))
                                }
                            } />
                            {
                                showConfirmPassword ? 
                                <img className='show-icon' src={HidePassImage} alt="Password Icon" onClick={HandelShowConfirmPassword} title="Hide Password" /> 
                                :
                                <img className='show-icon' src={ShowPassImage} alt="Password Icon" onClick={HandelShowConfirmPassword} title="Show Password" />
                            }
                        </div>
                        <button className='btn'>Sign Up</button>
                    </form>
                    <div className="feats">
                        <div className="auth-info">
                            Alreay have account? <Link to="/login">Sign in</Link>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Register;