import React, { useState } from "react";
import { useDispatch } from 'react-redux';
import { ForgetPasswordHandler } from "../../../redux/actions/Auth";
import { useHistory, Link } from "react-router-dom";
import PinInput from "react-pin-input";
import { CheckVerficationCodeHandler } from '../../../redux/actions/Auth';

const ForgetPassword = () => {

    const dispatch = useDispatch();

    const { push } = useHistory();

    const [ email, setEmail ] = useState('');

    const [ show, setShow ] = useState(false);

    const HandelSubmit = (e) => {
        e.preventDefault();
        dispatch(ForgetPasswordHandler(email, () => {
            localStorage.setItem('email', email)
            setShow(true)
        }))
    }

    return(
        <section className="auth">
            <div className="container">
                {
                    show ? 
                    <div className="auth-body">
                        <div className="auth-header">
                            <h1 className='title'>Verify Your Email </h1>
                        </div>
                        <div className='auth-form' style={{textAlign: "center"}}>
                            <PinInput 
                                length={6} 
                                initialValue=""
                                type="numeric" 
                                inputMode="number" 
                                onComplete={(value, index) => {
                                    dispatch(CheckVerficationCodeHandler(localStorage.getItem('email') ,value, () => {
                                        localStorage.setItem('code', value);
                                        push('/reset-password');
                                    }))
                                }}
                                autoSelect={true}
                                focus={true}
                                regexCriteria={/^[ 0-9_@./#&+-]*$/}
                            />
                        </div>
                        <p className="form-text">Please enter the verification code you received in your email.</p>
                        <div className="feats">
                            {/* <div className="auth-info">
                                Don't Receive Verification Code? <Link to="/">Resend Code</Link>
                            </div> */}
                        </div>
                    </div> 
                    :
                    <div className="auth-body">
                        <div className="auth-header">
                            <h1 className='title'>Reset Password </h1>
                            <p className='text'>Please enter your email address. You will receive a Code to create a new password via email.</p>
                        </div>
                        <form className='auth-form' onSubmit={HandelSubmit}>
                            <div className="form-group">
                                <input type="email" className="form-control" placeholder="Email" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setEmail(text);
                                }
                                } />
                            </div>
                            <button className='btn'>Reset Password</button>
                        </form>
                    </div>
                }
            </div>
        </section>
    )
}

export default ForgetPassword;