import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { ResetPasswordHandler } from "../../../redux/actions/Auth";
import { useHistory } from "react-router-dom";
import ShowPassImage from '../../../../public/assets/images/icons/showpass.svg';
import HidePassImage from '../../../../public/assets/images/icons/hidden.svg';

const ResetPassword = () => {

    const dispatch = useDispatch();

    const { push } = useHistory();

    const [ state, setState ] = useState({
        password : '',
        confirmPassword : ''
    })

    const [ showPassword, setShowPassword ] = useState(false);

    const [ showConfirmPassword, setShowConfirmPassword ] = useState(false);

    const HandelShowPassword = () => {
        setShowPassword(!showPassword);
    }

    const HandelShowConfirmPassword = () => {
        setShowConfirmPassword(!showConfirmPassword);
    }

    const HandelSubmit = (e) => {
        e.preventDefault();
        dispatch(ResetPasswordHandler(
            localStorage.getItem('email'),
            localStorage.getItem('code'),
            state.password,
            state.confirmPassword,
            () => {
                push('/login')
            }
        ))
    }

    return(
        <section className="auth">
            <div className="container">
                <div className="auth-body">
                    <div className="auth-header">
                        <h1 className='title'>Reset Password</h1>
                    </div>
                    <form className='auth-form' onSubmit={HandelSubmit}>
                        <div className="form-group">
                            <input type={ showPassword ? "text" : "password" } className="form-control" placeholder="New Password" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, password : text}))
                                }
                            } />
                            {
                                showPassword ? 
                                <img className='show-icon' src={HidePassImage} alt="Password Icon" onClick={HandelShowPassword} title="Hide Password" /> 
                                :
                                <img className='show-icon' src={ShowPassImage} alt="Password Icon" onClick={HandelShowPassword} title="Show Password" />
                            }
                        </div>
                        <div className="form-group">
                            <input type={ showConfirmPassword ? "text" : "password" } className="form-control" placeholder="Confirm Password" onChange={
                                (e) => {
                                    const text = e.target.value;
                                    setState((old) => ({...old, confirmPassword : text}))
                                }
                            } />
                            {
                                showConfirmPassword ? 
                                <img className='show-icon' src={HidePassImage} alt="Password Icon" onClick={HandelShowConfirmPassword} title="Hide Password" /> 
                                :
                                <img className='show-icon' src={ShowPassImage} alt="Password Icon" onClick={HandelShowConfirmPassword} title="Show Password" />
                            }
                        </div>
                        <button className='btn'>Reset Password</button>
                    </form>
                </div>
            </div>
        </section>
    )
}

export default ResetPassword;