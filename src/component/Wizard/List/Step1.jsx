import React, { useState, useEffect } from 'react'

const Step1 = ({Personal, data}) => {

    const [state, setState] = useState({
        username: "",
        email: "",
        phone: ""
    })

    useEffect(() => {
        Personal(state.username, state.email, state.phone)
    }, [state])

    return(
        <>
            <h3 className="step-title">Fill Personal Information</h3>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Name" defaultValue={data.Fname}
                    onChange={
                        (e) => 
                        {const text = e.target.value
                        setState((old) => ({...old , username : text }))}
                        }/>
                {/* <small className="form-text alert alert-danger">Name Is Required</small> */}
            </div>
            <div className="form-group">
                <input type="email" className="form-control" placeholder="Email" defaultValue={data.email}
                    onChange={
                        (e) => 
                        {const text = e.target.value
                        setState((old) => ({...old , email : text }))}
                        }/>
                {/* <small className="form-text alert alert-danger">Email is Required</small> */}
            </div>
            <div className="form-group">
                <input type="phone" className="form-control" placeholder="Phone" autoComplete="chrome-off" defaultValue={data.phone}
                    onChange={
                        (e) => 
                        {const text = e.target.value
                        setState((old) => ({...old , phone : text }))}
                    }
                />
                {/* <small className="form-text alert alert-danger">Phone Number Is Required</small> */}
            </div>
        </>
    )

}

export default Step1