import React, { useEffect, useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import PageHelmet from "../component/common/Helmet";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";
import WorkPageImage from '../../public/assets/images/pageheader/works.png';
import { useDispatch, useSelector } from "react-redux";
import { GetWorksPageDataHandler } from "../redux/actions/Works";


const Works = () =>  {

        const dispatch = useDispatch();

        const { WorksData } = useSelector((state) => state.worksData);

        const [model, setModel] = useState(false);
        
        const  [workData, setWorkData] = useState({
            title: "",
            link: ""
        })

        const [ pagination, setPagination ] = useState(9);

        const [ play, setPlay ] = useState(true)

        useEffect(() => {
            dispatch(GetWorksPageDataHandler());
        }, [dispatch])

        return (
            <>
                <PageHelmet pageTitle='Our Work' />
                <Header headertransparent="header--transparent" colorblack="color--black" logoname="logo.png" />
                <div className="rn-page-title-area pt--120 pb--190 bg_image"  data-black-overlay="5" style={{backgroundImage : `url(${WorkPageImage})`}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title" style={{color: '#a60808'}}>OUR WORK</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <main className="page-wrapper page-works">
                    <div className="container">
                        <div className="section-title mb-3">
                            <h2>Our Work</h2>
                            <p>Samples of Our Work</p>
                        </div>
                        <div className="row">
                            {
                                WorksData.slice(0,pagination).map(item => (
                                    <div className="col-md-4" key={item.id}>
                                        <div className="portfolio">
                                            <div className="content">
                                                <div className="inner">
                                                    <div className="thumb position-relative">
                                                        <img className="w-100" src={item.image} alt="Works Images" style={{ width: "400px", height: "400px" }}/>
                                                        <Button className="video-popup position-top-center md-size"  style={{backgroundColor: "#FBC723"}}
                                                            onClick={() => {
                                                                setModel(!model)
                                                                setWorkData({
                                                                    title: item.name,
                                                                    link: item.video
                                                                })
                                                                setPlay(false)
                                                            }}
                                                        >
                                                            <span className="play-icon"></span>
                                                        </Button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                        {
                            WorksData.length === pagination && 
                            <div className="load-more">
                                <button onClick={
                                    (e) => {
                                        e.preventDefault();
                                        setPagination(pagination + 9)
                                    }
                                }>Load More</button>
                            </div>
                        }
                    </div>
                    <Modal isOpen={model} toggle={() => setModel(!model)} className="works-modal"  >
                        <ModalHeader toggle={() => 
                            {
                                setModel(!model);
                                setPlay(true)
                            }
                        }>
                            {workData.title}
                        </ModalHeader>
                        <ModalBody>
                            <iframe title="vimeo-player" src={workData.link} width="100%" height="360" frameBorder="0" allowFullScreen></iframe>
                        </ModalBody>
                    </Modal>
                </main>
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                <Footer />               
            </>
        )
    }
    
export default Works;