import React from "react";
import PageHelmet from "../component/common/Helmet";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";
import Work from "../component/Work";
import WorkPageImage from '../../public/assets/images/pageheader/works.png';


const Works = () =>  {

        return (
            <>
                <PageHelmet pageTitle='Our Work' />
                <Header headertransparent="header--transparent" colorblack="color--black" logoname="logo.png" />
                <div className="rn-page-title-area pt--120 pb--190 bg_image"  data-black-overlay="5" style={{backgroundImage : `url(${WorkPageImage})`}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title" style={{color: '#a60808'}}>OUR WORK</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <main className="page-wrapper">
                    <Work />
                </main>
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                <Footer />               
            </>
        )
    }
    
export default Works;