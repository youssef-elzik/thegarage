import React, { useEffect } from "react";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";
import PortfolioList from "../elements/portfolio/PortfolioList";
import CallAction from "../elements/callaction/CallAction";
import ContactOne from '../elements/contact/ContactOne'
import HomeSlider from "../elements/HomeSlider";
import HomeAbout from "../elements/HomeAbout";
import Work from "../component/Work";
import { useDispatch, useSelector } from "react-redux";
import { GetInformationDataHandler } from "../redux/actions/Information";
import { Link } from "react-router-dom";


const Home = () => {

    const dispatch = useDispatch();

    const { ServicesData, Information } = useSelector((state) => state.homeData);
    
    useEffect( () => {
        dispatch(GetInformationDataHandler())
    }, [dispatch])

    
    return(
        <> 
            <Header logo="light" colorblack="color--black"/>
            <HomeSlider title="The Garage" description="WE CREATE IMMERSIVE AUDIO EXPERIENCES" bg={Information.home_banner}/>
            {/* <HomeSlider title="The Garage" description="We Make Mountains of Sound out of THE GARAGE." bg={Information.home_banner}/> */}
            <div className="portfolio-area ptb--120 bg_image bg_image--3">
                <div className="portfolio-sacousel-inner">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="section-title text-left service-style--3 mb--15">
                                    <h2 className="title">Our Services</h2>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <PortfolioList services={ServicesData} />
                        </div>
                    </div>
                </div>
            </div>
            <HomeAbout title="About Us" subTitle={Information.home_about_hint} description={Information.home_about_body} img={Information.home_about_image} />
            <Work  />
            <div className="works-link text-center">
                <Link to="/works" className='rn-button-style--2 btn_solid mt-1'>See All Works</Link>
            </div>
            <CallAction />
            <ContactOne 
                address={Information.address} 
                email={Information.email} 
                phone={Information.phone}
                facebook={Information.facebook}
                instagram={Information.linkedin}
                vimeo={Information.twitter}
            />
            <Footer />
            <div className="backto-top">
                <ScrollToTop showUnder={160}>
                    <FiChevronUp />
                </ScrollToTop>
            </div>
        </>
    )
}


export default Home;