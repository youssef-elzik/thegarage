import React, {useState, useEffect} from 'react';
import Slider from "react-slick";
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { GetWorksPageDataHandler } from '../../redux/actions/Works';

const Work = () => {
    

    let title = 'Our Work',
        description = 'Samples of Our Work';

        const dispatch = useDispatch();

        const { WorksData } = useSelector((state) => state.worksData);

        const [model, setModel] = useState(false);
        
        const  [workData, setWorkData] = useState({
            title: "",
            link: ""
        })

        const [ play, setPlay ] = useState(true)

        const portfolioSlick = {
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: play,
            autoplaySpeed: 2000,
            dots: true,
            arrows: true,
            responsive: [{
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 993,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
        };

        useEffect(() => {
            dispatch(GetWorksPageDataHandler());
        }, [dispatch])

    return(
        <div className="portfolio-area ptb--120 bg_image bg_image--3">
            <div className="portfolio-sacousel-inner mb--55">
            <div className="rn-section ">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="section-title">
                                <h2>{title}</h2>
                                <p>{description}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="portfolio-slick-activation mt--70 mt_sm--40 cust-slider">
                    <Slider {...portfolioSlick}>     
                        {
                            WorksData.map(item => (
                                    <div className="portfolio" key={item.id}>
                                        <div className="content">
                                            <div className="inner">
                                            <div className="thumb position-relative">
                                                <img className="w-100" src={item.image} alt="Works Images" style={{ width: "400px", height: "400px" }}/>
                                                <Button className="video-popup position-top-center md-size"  style={{backgroundColor: "#FBC723"}}
                                                    onClick={() => {
                                                        setModel(!model)
                                                        setWorkData({
                                                            title: item.name,
                                                            link: item.video
                                                        })
                                                        setPlay(false)
                                                    }}
                                                >
                                                    <span className="play-icon"></span>
                                                </Button>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }    
                    </Slider>
                    <Modal isOpen={model} toggle={() => setModel(!model)} className="works-modal"  >
                        <ModalHeader toggle={() => 
                            {
                                setModel(!model);
                                setPlay(true)
                            }
                        }>
                            {workData.title}
                        </ModalHeader>
                        <ModalBody>
                            <iframe title="vimeo-player" src={workData.link} width="100%" height="360" frameBorder="0" allowFullScreen></iframe>
                        </ModalBody>
                    </Modal>
                </div>
            </div>
            </div>
        </div>
    )
}

export default Work